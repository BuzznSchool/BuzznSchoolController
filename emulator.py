"""
Emulates a controller
FOR DEBUGGING ONLY

@author TheDoctor
@version 0.1
@date 8.7.17
"""

from connection import Connection
import sys

class Emulator:

    conn = None
    getchar = None

    def run(self):
        bAdmin = False
        if len(sys.argv) == 2:
            if sys.argv[1] == "admin":
                bAdmin = True
        self.conn = Connection(bAdmin)
        self.getchar = _Getch()

        # Main loop
        while True:
            c = self.getchar()

            if c == "w":
                self.conn.sendButtonDown("up")
            elif c == "a":
                self.conn.sendButtonDown("left")
            elif c == "s":
                self.conn.sendButtonDown("down")
            elif c == "d":
                self.conn.sendButtonDown("right")

            elif c == "1":
                self.conn.sendButtonDown("1")
            elif c == "2":
                self.conn.sendButtonDown("2")
            elif c == "3":
                self.conn.sendButtonDown("3")
            elif c == "4":
                self.conn.sendButtonDown("4")

            elif c == "#":
                exit(0)

            else:
                print("Unknown: " + c)

class _Getch:
    """Gets a single character from standard input.  Does not echo to the screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()

if __name__ == "__main__":
    Emulator().run()