"""
Runs on device's startup
Prepares the controller

@author TheDoctor
@version 0.1
@date 02.07.17
"""

import network      # Setting up wifi

# Connection details
essid = "testMasterWifi"
wifiPass = "supersecretpassDOOM"

def do_connect():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        sta_if.active(True)
        sta_if.connect(essid, wifiPass)
        while not sta_if.isconnected():
            pass

def close_ap():
    ap_if = network.WLAN(network.AP_IF)

    if ap_if.active():
        ap_if.active(False)

close_ap()
do_connect()