"""
Holds every connection related variable

@author TheDoctor
@version 0.1
@date 8.7.17
"""

class ConnectionConstants:

    server_address = ('localhost', 1337)

    normalPacket = 0x69
    lastPacket = 0xff
    clientAuthenticatedPacket = 0x42

    lenDataChunk = 256
    lenClientID = 20
    defClientIDval = 0x66
    defAdminIDval = 0xaa

    buzzerTypeMultipleButtons = 0xf4

    # Used by emulator
    buttonBytes =  {
            "1" : 0x01,
            "2" : 0x02,
            "3" : 0x03,
            "4" : 0x04,
            "left" : 0x05,
            "right" : 0x06,
            "up" : 0x07,
            "down" : 0x08}