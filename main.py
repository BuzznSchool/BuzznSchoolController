"""
Runs after setup (boot.py) has finished
Handles button presses

@author TheDoctor, Jabbi
@version 0.1
@date 8.7.17
"""
from button_const import button_translation
from connection import Connection
import machine
import time


class Main(object):
    conn = None
    data_latch = machine.Pin(5, machine.Pin.OUT)
    clock = machine.Pin(0, machine.Pin.OUT)
    data_out = machine.Pin(4, machine.Pin.IN, machine.Pin.PULL_UP)
    old_pressed = []

    def get_all_inputs(self):
        buffer_pressed = []
        self.data_latch.on()
        time.sleep(.004)
        self.data_latch.off()
        time.sleep(.004)
        for i in range(0, 16):
            self.clock.off()
            time.sleep(.004)
            if i < 12:
                if self.data_out.value() == 0:
                    print("[*] add button to old ")
                    buffer_pressed.append(button_translation[i])
            self.clock.on()
            time.sleep(.004)
        return buffer_pressed

    def is_in_old_pressed(self, button):
        return self.is_in_list(self.old_pressed, button)

    @staticmethod
    def is_in_list(list_to_check, item):
        for i in list_to_check:
            if i == item:
                return True
        return False

    def run(self):
        self.conn = Connection()
        self.old_pressed = []

        # Main loop
        while True:
            pressed = self.get_all_inputs()
            if len(self.old_pressed) > 0:
                print("[*] The pressed buttons: " + str(self.old_pressed))
            for button in pressed:
                print("pls")
                if not self.is_in_old_pressed(button):
                    print("[*] Add button to old_pressed: "+str(button))
                    self.old_pressed.append(button)
                    print("[*] Send ButtonDown "+str(button))
                    self.conn.sendButtonDownWithByte(button)
            for button in self.old_pressed:
                if not self.is_in_list(pressed, button):
                    print("[*] Remove button: " + str(button))
                    self.old_pressed.remove(button)

if __name__ == '__main__':
    Main().run()
