"""
Contains every connection related functions
Used for sending/receiving packets

@author TheDoctor
@version 0.1
@date 8.7.17
"""

import socket
from connectionConstants import ConnectionConstants

class Connection:

    controllerName = "FluffelFeli"
    bAdmin = None
    clientID = None
    sock = None

    def __init__(self, bAdmin):
        self.bAdmin = bAdmin
        while True:
            try:
                if (self.bAdmin == True):
                    self.clientID = []
                    for i in range(20):
                        self.clientID.append(ConnectionConstants.defAdminIDval)
                    break
                self.getClientID()
                break
            except:
                pass

    def connect(self):
        if self.sock != None:
            self.sock.close()
        self.sock = socket.create_connection(ConnectionConstants.server_address)

    def getClientID(self):
        self.connect()
        self.clientID = []
        for i in range(20):
            self.clientID.append(ConnectionConstants.defClientIDval)

        packet = []
        packet.extend(self.clientID)
        packet.extend(list(self.controllerName.encode("utf-8")))
        self.sendMessage(packet)
        if (self.bAdmin == False):
            self.clientID = self.readMessage()[:20]

    # Used by emulator
    def sendButtonDown(self, buttonName):
        self.handshake()
        self.sendPacket(
            ConnectionConstants.buzzerTypeMultipleButtons,
            ConnectionConstants.buttonBytes[buttonName])

    def sendButtonDownWithByte(self, buttonByte):
        """takes as param buttonBytes instead of string with button name"""
        self.handshake()
        self.sendPacket(
            ConnectionConstants.buzzerTypeMultipleButtons,
            buttonByte)

    def sendPacket(self, flag, data):
        packet = []
        packet.append(flag)
        packet.append(data)
        self.sendMessage(packet)

    def handshake(self):
        self.connect()
        self.sendMessage(self.clientID)

        if self.readMessage()[0] != ConnectionConstants.clientAuthenticatedPacket:
            print("Invalid ID...")
            self.getClientID()

    def sendMessageByte(self, byte):
        byteArray = bytearray()
        byteArray.append(byte)
        self.sendMessage(byteArray)

    def sendMessage(self, data):
        i = 0
        while i < len(data):
            packet = bytearray()
            if i + ConnectionConstants.lenDataChunk < len(data):
                packet.append(ConnectionConstants.normalPacket)
                packet.extend(data[i:i + ConnectionConstants.lenDataChunk])
            else:
                packet.append(ConnectionConstants.lastPacket)
                packet.extend(data[i:len(data)])

            self.sock.sendall(packet)

            i += ConnectionConstants.lenDataChunk

    def readMessage(self):
        data = []
        rawData = self.sock.recv(ConnectionConstants.lenDataChunk + 1)
        while rawData[0] != ConnectionConstants.lastPacket:
            data.append(rawData[1:ConnectionConstants.lenDataChunk + 1])
            rawData = self.sock.recv(ConnectionConstants.lenDataChunk + 1)
        data.append(rawData[1:ConnectionConstants.lenDataChunk + 1])

        endData = []
        for i in range(len(data)):
            for j in range(len(data[i])):
                endData.append(data[i][j])

        return endData